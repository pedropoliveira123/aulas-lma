//Processamento dos dados  do formulário "index.html"

//Acessando os elemento formulário do html
const formulario = document.getElementById("formulario1");

//adiciona um ouvinte de eventos a um elemento HTML
formulario.addEventListener("submit", function(evento)
{
    evento.preventDefault();/*previne o comportamento padrão de um elemento HTML em resposta a um evento*/

    //variáveis para tratar os dados recebidos dos elementos do formulário
    const nome = document.getElementById("nome").value;
    const email = document.getElementById("email").value;

    //Exibe um alerta com os dados
    //alert(`Nome: ${nome} --- E-mail: ${email}`);

    const updateresultado = document.getElementById ("resultado");

    updateresultado.value = `Nome: ${nome} E-mail: ${email}`;

    updateresultado.style.width = updateresultado.scrollWidth + "px";
});