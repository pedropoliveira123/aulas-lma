const {createApp} = Vue;
createApp ({
    data(){
        return{
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[
                './imagens/lua.jpg',
                './imagens/SENAI_logo.png',
                './imagens/sol.jpg'
            ],

            imagensInternet:[
                'https://p2.trrsf.com/image/fget/cf/774/0/images.terra.com/2022/11/04/95040434-ee2a27a16fc19e161008b48f351a35aa.jpg',
                'https://www.gp1.com.br/media/image_bank/2023/3/mesut-ozil-se-aposenta-do-futebol-aos-34-anosnone.jpg.1200x0_q95_crop.webp',
                'https://static-img.zz.pt/history/imgS620I12264T20200519150859.jpg'
            ],

        };//fim return
    },//fim data

computed:{
    randomImage()
    {
        return this.imagensLocais[this.randomIndex];
    },//fim randomImage

    randomImageInternet()
    {
        return this.imagensInternet[this.randomIndexInternet];
    }//fim randomImageInternet
},//fim computed

methods:{
    getRandomImage()
    {
        this.randomIndex = Math.floor(Math.random()*this.imagensLocais.length);

        this.randomIndexInternet = Math.floor(Math.random()*this.imagensInternet.length);
    }
},//fim methods

}).mount("#app");