const {createApp} = Vue;
createApp({
    data(){
        return{
            username:'',
            password:'',
            error: null,
            sucesso: null,

            //arrays para armazenamento dos usuários e senhas
            usuarios: ["Pedro", "Roger Guedes"],
            senhas: ["1234", "1234"],

            //variáveis para armazenamento de usuário e senha que será cadastrado
            newUsername: "",
            newPassword: "",
            confirmPassword: "",

            //Interação das mensagens de texto
            mostrarEntrada: false,
        };// fim return
    },// fim data

    methods: {
        login(){
            setTimeout(() => {
                if((this.username === 'Matheus Bidu' && this.password === '123456') ||  (this.username === 'Renato Augusto' && this.password === '12345678')){
                        alert ('Login realizado ')
                } //fim do if
                else{
                    this.error = "Nome ou senha incorretos!"
                };
            }, 1000);
        }, // fechamento Login

        loginArray(){
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;

                //uso de uma constante para armazenar o número do index onde o usuário "procurado" está "guardado" no array
                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                    this.error = null;
                    this.sucesso = "Login efetuado com sucesso!";

                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);

                }//Fechamento if

                else{
                    this.sucesso = null;
                    this.error = "Usuário ou senha incorretos!";
                }//Fechamento else

                this.username = "";
                this.password = "";

            }, 500);
        }, //Fechamento LoginArray

        adicionarUsuario(){
            if(!this.usuarios.includes(this.newUsername) && this.newUsername !== ""){
                if(this.newPassword && this.newPassword === this.confirmPassword){
                    this.usuarios.push(this.newUsername);
                    this.senhas.push(this.newPassword);

                    //Atualizando os valores dos arrays no armazenamento local
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));

                    this.newUsername = "";
                    this.newPassword = "";
                    this.confirmPassword = "";

                    this.sucesso = "Usuário cadastrado com sucesso!";
                    this.error = null;
                }//Fechamento if
                else{
                    this.error = "Por favor, digite uma senha válida!";
                    this.sucesso = null;
                }
            }//fechamento if
            else{
                this.error = "Por favor, informe um usuário válido!";
                this.sucesso = null;
            }//Fechamento else
        },//Fechamento adicionarUsuario
    }, // fechamento methods
}).mount("#app");// fechamento createApp